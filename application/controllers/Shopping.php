<?php
	class Shopping extends CI_Controller{
		
		public function index(){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->model("shopping_model");
			
			$shoppings = $this->shopping_model->getUserShoppings($this->session->userdata("id"));
		
			$data['shoppings'] = $shoppings;
		
			$this->load->template("shopping/view", $data);
		}
		
		public function add(){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->library("form_validation");
						
			$this->form_validation->set_rules("name[0]", "Nom", "required|min_length[2]|max_length[200]", array(
				"required"		=> "Vous devez spécifier au moins un achat !",
				"min_length"	=> "Le nom doit contenir 2 caractères au minimum !",
				"max_length"	=> "Le nom ne peut pas contenir plus de 200 caractères !"
			));
			
			$this->form_validation->set_rules("category[]", "Catégorie", "required|callback__ownCategory", array(
				"required"		=> "Vous devez spécifier une catégorie !",
				"_ownCategory"	=> "Catégorie non trouvée !"
			));
			
			$this->form_validation->set_rules("price[]", "Prix", "required|numeric", array(
				"required"		=> "Vous devez spécifier le prix !",
				"numeric"		=> "Le prix spécifié n'est pas un nombre valide !"
			));
			
			$this->form_validation->set_rules("quantity[]", "Quantité", "required|numeric", array(
				"required"		=> "Vous devez spécifier la quantité !",
				"numeric"		=> "La quantité spécifiée n'est pas un nombre valide !"
			));
			
			$this->form_validation->set_rules("name[]", "Nom", "required|min_length[2]|max_length[200]", array(
				"required"		=> "Vous devez spécifier un nom pour tout les achats !",
				"min_length"	=> "Le nom doit contenir 2 caractères au minimum !",
				"max_length"	=> "Le nom ne peut pas contenir plus de 200 caractères !"
			));
			
			if($this->form_validation->run() == FALSE){
				$this->load->model("category_model");
				$categories = $this->category_model->getAllOfUser($this->session->userdata("id"));
				$data['categories'] = $categories;
				$this->load->template("shopping/new_item_form", $data);
			}
			else{
				$this->load->model("shopping_model");
				$this->load->model("category_model");
				$this->load->library("email");
				
				$names = $this->input->post("name");
				$categories	= $this->input->post("category");
				$prices = $this->input->post("price");
				$quantities = $this->input->post("quantity");
				
				$items = array();
				
				foreach($names as $key => $name){
					array_push($items, array(
						"name"		=> $name,
						"category"	=> $categories[$key],
						"price"		=> $prices[$key],
						"quantity"	=> $quantities[$key]
					));
					$oldTotal = $this->shopping_model->getTotalPriceOfACategory($categories[$key], $this->session->userdata("id"));
					$category = $this->category_model->getCategory($categories[$key], $this->session->userdata("id"));
					
					if(($oldTotal + $quantities[$key] * $prices[$key]) >= $category->price_limit){
						$this->email->initialize(array(
							"protocol"		=> "smtp",
							"smtp_host"		=> "casarray1.hq.k.grp",
							"mailtype"		=> "html"
						));
					
					
						$this->email->set_newline("\r\n");  
						$this->email->set_crlf( "\r\n" );
						$this->email->to($this->session->userdata("mail"));
						$this->email->from("robot@budgetmanager.com");
						$this->email->subject("Limite de prix atteinte !");

						$data['category'] = $category;
						
						$this->email->message($this->load->view("mails/price_limit", $data, true));
						
						$this->email->send();
						
					}
				}
				
				$this->shopping_model->addItems($items, $this->session->userdata("id"));
				
				redirect("categories");
			}
		}
		
		public function _ownCategory($catId){
			$this->load->model("category_model");
			
			return $this->category_model->userOwnCategory($this->session->userdata("id"), $catId);
		}
	}
?>