<?php
class Devtools extends CI_Controller{
	
	public function reset_DB(){
		$this->load->library("redbean");
		
		R::nuke();
		
		$user = R::dispense("users");
		$user->mail = "";
		$user->firstname = "";
		$user->lastname = "";
		$user->hash = "";
		$user->confirmation = "";
		$user->admin = "";
		
		R::store($user);
		
		$categorie = R::dispense("categories");
		$categorie->name = "";
		$categorie->color = "";
		$categorie->image_data = "";
		$categorie->price_limit = "";
		$categorie->user = $user;
		
		R::store($categorie);
		
		$shopping = R::dispense("shopping");
		$shopping->date = "";
		$shopping->user = $user;
		
		R::store($shopping);
		
		$item = R::dispense("items");
		$item->name = "";
		$item->price = "";
		$item->quantity = "";
		$item->categories = $categorie;
		$item->shopping = $shopping;
		
		R::store($item);
	}
}