-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2016 at 05:35 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `budget_manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(7) NOT NULL,
  `image_data` longtext,
  `fk_users` int(11) NOT NULL,
  `name` text NOT NULL,
  `price_limit` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `price_limit` (`fk_users`),
  KEY `fk_users` (`fk_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `color`, `image_data`, `fk_users`, `name`, `price_limit`) VALUES
(22, '#5f52ad', NULL, 42, '24dbb91f27fdf51aa26e15af9c48a8185d4b244e113e46d11b1d1ec52f3ddfb28d63679b1a8e3f01aab6ddeff01b61f3107c9621fd1c0734461c38967d5e066cHHEFbP28vrA/kLnj3aJNyKBnFmhtaw==', '6e8cb9a0c3df7e2e3565bb93a1894290aad4153221be9d8d6db8728c9e5c60a87bd50b53a8d2bddc2dc44620119761500579e55afb83298f3e970219b12a2b38MDzoO0+ztklGXYK1nGLkavpY'),
(23, '#b0aa24', NULL, 41, '2d54c7de17c47989ce6c9ceccd0e2c6d94f34e81c71638758e3410732119e67122b3245adae700ac2cd3ab30838a99c4447a1fe0e1f1d80d012603b004f9f47fNz5caVstJdkGhv3FRe+6FrBji4h1mh5upCo=', 'e23babb422bc0dc6ef6228d58d790bddb13ddd7c16b733d38e1dcad4694864e17b2dfedf7fcc7f0dd996665511cefe6641ad1fd21b64c01b384238b74fd319f1wHVutALl3fFC/efD+HUxwxE='),
(24, '#16a2cf', NULL, 41, '1ceb735cf2fb83242f40eec62db06850f77f7c985a5855b9fa91670d42c834c0ecc51406d4d4fe21f742073bd89d0f86014448e6c7fb81be71a82b67ea1e0b8ePoxMjLVcUkCbQFTpl+e9lwatqoI=', '6d753302842e4064f0932bc5afd3e14ebe9d65037ff0624b9536c11539799b7bb23234b43603802f06f93df8aa75a8b5de1e80025d6f2243882b68bec21a7bd9ItfI+uDf102C4+qlz/NDJaQ=');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `price` text NOT NULL,
  `quantity` text NOT NULL,
  `fk_categories` int(11) NOT NULL,
  `fk_shopping` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories` (`fk_categories`),
  KEY `fk_categories_2` (`fk_categories`),
  KEY `fk_shopping` (`fk_shopping`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `price`, `quantity`, `fk_categories`, `fk_shopping`) VALUES
(7, 'e6b5c964b18cffee5210ce5dd817d6e8161c3ce621241fd8df841a753ef93efe928ecebc820948eebf9ab391341e41ad708a053dbd4eb992e07edf370d857df4ybtWsXwf8nzDJuXo8V2cqwcStnofrK2S', 'a16c4bf063c04233af92a8946cdfc0fbfe92768771f051c9cf9bdc905b9041c203df2db82ec5fed6121ffef840b6b052493951eeeca3e13c86fb78c333f7eb8fBF+iFanA7C+Ml4dxqbQPgIg=', '70b84f73450e82fe3a7bad3fc6d449749b3c12b88fc6f71a1e484d85a77b2d6d4255c267028bbe3c9e1c761f30704df336e04a323a16553434cd0a81c8ea4330O58PjsJ9D8KsJPx0QrwD5cI=', 23, 7),
(8, 'e24c0a1205b7770906cec3eabd07d48f3dfbc7053503c84294acf37b64d058e6d483b2f579485b1f13f37d616ee08038ede522016f7ec7e623a4e694da285821dOitqNt4AILHQeUdc2hTHTQiLfDA', '04862df479af1ba75bbef07543c788764df74e2445f1772a925ad01c0aa29b2acdf25c0572a7864fec1f931ff7820d01f32a7284d27fb76465956996f3390606dRNP6zb0oqSsc2LOntkWdJo=', 'a2d1fef3ca9600e6edd0eca0cfa377a5ce12bff9a45ecc2cf4892438ddfe014d084155045695c60ac623edfab9d82ed471d6ffe17ac596d21672de0a6adce150YRDeCMhcBxh6gZa/lbI9Jhs=', 23, 7),
(9, 'c71443f19ecd6755040744e3e308e9e9b56c0744efdc86fdd861dac791aec27841f9d9aadb53ebe9383854a5e46d7db6c702dc2f48425371ebb23ed14334e9e8sYzmpB62L6eP9/qrZjCSMcyAc0Y=', '8d4951a128257abde4669da604f42a7208e853cbe13b58f05b5cd92e34b1405f2480d94948c4a4e531186f8af5a6fda8cd94b791304f87dc32ac52283937e6e6IH9jLBWj7arA5wvh0ZWaFexiVQ==', '8b2cf14e7c5cdef79cf1c3b1ddeba2e8f7295972e18e0ec1dc5fdeba86b87c612dcddc12648979922831beaee213f6a0f67ee19c520bc9b98d938c46c4a8319aGGbhS2u2ezPMaSmKdkosxm4=', 24, 8),
(10, '94442748cdd10ced329739b74b812367bf9b536e1ffaf693dc763b978285d8c942aea55ef0cea360709bb38511cc44a7b27b5aa00e11d218fc6b93f9e44fd41aDRd1qGr03PDgMOnVpRTZsjC0xjPgrQ==', '4d0fa3b9f534879c5335b7f9fb417a9c8aef01f9942d4bff17606358c790472f120805152980b54a43511f263c2b2db2c89b4ef369aab56d4a952237b5bc29becMS8CBKUSHMM82BkW5EUkyzf', '17d1dac2c2847eb4ef6316eafee32501313d084233963e9a9d55de22f0141553717772fe41aa1f0d1e28db278cefe1c2e7d9f205aa854b96345df6d1ad3cd727DCcKIzikm5KPmDGeN6kbbvs=', 23, 9);

-- --------------------------------------------------------

--
-- Table structure for table `shopping`
--

CREATE TABLE IF NOT EXISTS `shopping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` bigint(20) NOT NULL,
  `fk_users` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users` (`fk_users`),
  KEY `fk_users_2` (`fk_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `shopping`
--

INSERT INTO `shopping` (`id`, `date`, `fk_users`) VALUES
(6, 1453296952, 41),
(7, 1453298946, 41),
(8, 1453299201, 41),
(9, 1453307578, 41);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `confirmation` varchar(200) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `mail`, `firstname`, `lastname`, `hash`, `confirmation`, `admin`) VALUES
(41, 'guillaume.verdon@nagra.com', 'Guillaume', 'Verdon', '74436384ee385cd58a155e5ee80283d70495aa9f', '1', 1),
(42, '187guigui@gmail.com', 'Guillaume', 'Verdon', '6d6b82d86ad86ea13225e55320d6816e5d91e5c7', '1', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`fk_categories`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_ibfk_2` FOREIGN KEY (`fk_shopping`) REFERENCES `shopping` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shopping`
--
ALTER TABLE `shopping`
  ADD CONSTRAINT `shopping_ibfk_1` FOREIGN KEY (`fk_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
