﻿<?php
	class Register extends CI_Controller{
		public function index(){
			$this->load->helper("user");
			needPrivileges(array("logged_out"));
			$this->load->library("recaptcha");
			$data['captcha'] = $this->recaptcha->getWidget();
			$data['captcha_linker'] = $this->recaptcha->getScriptTag();
			$this->load->template("registration/registration_form", $data);
		}
		
		public function submit(){
			$this->load->helper("user");
			needPrivileges(array("logged_out"));
			$this->load->library("form_validation");
			$this->load->library("email");
			$this->load->model("user_model");
			$this->load->helper("url");
			$this->load->library("recaptcha");
			
			$recaptcha = $this->input->post('g-recaptcha-response');
			$response = $this->recaptcha->verifyResponse($recaptcha);
			
			if(isset($response['success']) and $response['success'] === true){
				
				$this->form_validation->set_rules("mail", "Adresse e-mail", "required|valid_email|callback__mailIsUnique", array(
					"required"					=> "Vous devez spécifier votre adresse email",
					"valid_email"				=> "L'adresse spécifiée n'est pas valide !",
					"_mailIsUnique" 			=> "L'adresse spécifiée est déjà associée à un autre compte !"
				));
				
				$this->form_validation->set_rules("firstname", "Prénom", "required|min_length[2]|max_length[200]", array(
					"required"					=> "Vous devez spécifier votre prénom !",
					"min_length"				=> "Votre prénom doit contenir au minimum 2 caractères !",
					"max_length"				=> "Votre prénom doit contenir au maximum 200 caractères !"
				));
				
				$this->form_validation->set_rules("lastname", "Nom", "required|min_length[2]|max_length[200]", array(
					"required"					=> "Vous devez spécifier votre nom !",
					"min_length"				=> "Votre nom doit contenir au minimum 2 caractères !",
					"max_length"				=> "Votre nom doit contenir au maximum 200 caractères !"
				));
				
				$this->form_validation->set_rules("password", "Mot de passe", "required", array(
					"required"					=> "Vous devez choisir un mot de passe !"
				));
				
				$this->form_validation->set_rules("confirmation", "Confirmation", "required|matches[password]", array(
					"required"					=> "Vous devez remplir le champs confirmation !",
					"matches"					=> "Le champs confirmation doit correspondre au mot de passe !"
				));
				
				$this->form_validation->set_rules("cgu", "CGU", "required|callback__verifyCGU", array(
					"required"					=> "Vous devez accepter les CGU !",
					"_verifyCGU"				=> "Vous devez accepter les CGU !"
				));
							
				if($this->form_validation->run() == FALSE){
					$data['captcha'] = $this->recaptcha->getWidget();
					$data['captcha_linker'] = $this->recaptcha->getScriptTag();
					$this->load->template("registration/registration_form", $data);
				}
				else{
					$mail = $this->input->post("mail");
					$firstname = $this->input->post("firstname");
					$lastname = $this->input->post("lastname");
					$password = $this->input->post("password");
					
					$confirmToken = $this->user_model->register($mail, $firstname, $lastname, $password);
					$confirmLink = site_url("register/confirmation")."/".$confirmToken;
					
					$this->email->initialize(array(
						"protocol"		=> "smtp",
						"smtp_host"		=> "casarray1.hq.k.grp",
						"mailtype"		=> "html"
					));
					
					
					$this->email->set_newline("\r\n");  
					$this->email->set_crlf( "\r\n" );
					$this->email->to($mail);
					$this->email->from("robot@budgetmanager.com");
					$this->email->subject("Confirmez votre inscription !");
					
					$data = array(
						"confirmLink"	=> $confirmLink
					);
					$this->email->message($this->load->view("mails/confirmation_link", $data, true));
					
					if($this->email->send()){
						$this->load->template("registration/registration_sent");
					}
				}
			}
			else{
				redirect("/register");
			}
		}
		
		public function confirmation($token){
			$this->load->model("user_model");
			$this->user_model->verify($token);
			
			$this->load->template("registration/registration_success");
		}
		
		public function _verifyCGU(){
			if($this->input->post("cgu")){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function _mailIsUnique($mail){
			$this->load->model("user_model");
			return $this->user_model->userIsUnique($mail);
		}
	}
?>