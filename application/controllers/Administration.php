﻿<?php
	class Administration extends CI_Controller{
		
		public function index(){
			$this->load->helper("url");
			
			redirect("/administration/users");
		}
		
		public function users(){
			$this->load->helper("user");
			
			needPrivileges(array("logged_in", "admin"));
			
			$this->load->model("user_model");
			
			$users = $this->user_model->getAll();
			
			$data['users'] = $users;
			
			$this->load->template("administration/users_list", $data);
		}
	}
?>