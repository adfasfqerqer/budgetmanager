<?php
	class Reports extends CI_Controller{
		public function index($time = "year"){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->model("category_model");
			$this->load->model("shopping_model");
			
			$this->load->library("encryption");
			
			$shoppings = $this->shopping_model->getUserShoppings($this->session->userdata("id"));
			
			$categories = $this->category_model->getAllOfUser($this->session->userdata("id"));
			
			switch($time){
				case "year":
				$shoppings = array_filter($shoppings, array($this, "filterYear"));
				break;
				
				case "month":
				break;
				
				case "week":
				$shoppings = array_filter($shoppings, array($this, "filterWeek"));
				break;
				
				default:
				$shoppings = array_filter($shoppings, array($this, "filterYear"));
				break;
			}
			
			$total = 0;
			foreach($shoppings as $shopping){
				foreach($shopping->ownItemsList as $item){
					 $total += $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) * $this->encryption->my_decrypt($this->session->userdata("id"), $item->price);
				}
			}
			
			$totalPerMonth = $this->findTotalsByMonth($shoppings);
			
			$data['categories'] = $categories;
			$data['shoppings'] = $shoppings;
			$data['sampling'] = $time;
			$data['total'] = $total;
			$data['totalPerMonth'] = $totalPerMonth;
			
			$this->load->template("categories/categories", $data);
		}
		
		public function category($categoryId){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->model("category_model");
			$category = $this->category_model->getCategory($categoryId, $this->session->userdata("id"));
			
			if($category->users->id != $this->session->userdata("id")){
				redirect("/");
			}
			
			$totalByMonth = $this->findTotalsByMonth(array($category));
			
			$data['category'] = $category;
			$data['totalPerMonth'] = $totalByMonth;
			
			$this->load->template("categories/reports", $data);
		}
		
		private function filterYear($shopping){
			if(date("Y", $shopping['date']) == date("Y")){
				return true;
			}
			else{
				return false;
			}
		}
		
		private function filterMonth($shopping){
			if(date("n", $shopping['date']) == date("n")){
				return true;
			}
			else{
				return false;
			}
		}
		
		private function filterWeek($shopping){
			if(date("W", $shopping['date']) == date("W")){
				return true;
			}
			else{
				return false;
			}
		}
		
		private function findTotalsByMonth($shoppings){
			$totals = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			
			for($i = 1; $i <= 12; $i++){
				foreach($shoppings as $shopping){
					if(date("n", $shopping->date) == $i){
						$total = 0;
						foreach($shopping->ownItemsList as $item){
							$total += $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) * $this->encryption->my_decrypt($this->session->userdata("id"), $item->price);
						}
						$totals[$i -1] += $total;
					}
				}
			}
			return $totals;
		}
	}
?>