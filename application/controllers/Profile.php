﻿<?php
	class Profile extends CI_Controller{
		
		public function index(){
			$this->load->helper("url");
			redirect("/profile/view");
		}
		
		public function view($id = null){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->model("user_model");
			if($id != null){
				needPrivileges(array("admin"));
				$users = $this->user_model->getUserById($id);
				$data['user'] = $users;
				$data['id'] = $id;
				$data['admin'] = true;
			}
			else{
				$users = $this->user_model->getUserByMail($this->session->userdata("mail"));
				$data['user'] = $users[0];
				$data['admin'] = false;
				$data['id'] = null;
			}
			$this->load->template("profile/view", $data);
		}
		
		public function edit($id = null){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			if($id != null){
				needPrivileges(array("admin"));
			}
			
			$this->load->library("form_validation");
			$this->load->model("user_model");
			
			if($id != null){
				$targets = $this->user_model->getUserById($id);
				$target = $targets;
			}
			
			$this->form_validation->set_rules("firstname", "Prénom", "required|min_length[2]|max_length[200]", array(
				"required"		=> "Vous devez spécifier le prénom !",
				"min_length"	=> "Le prénom doit contenir au minimum 2 caractères !",
				"max_length"	=> "Le prénom doit contenir au maximum 200 caractères !"
			));
			
			$this->form_validation->set_rules("lastname", "Nom", "required|min_length[2]|max_length[200]", array(
				"required"		=> "Vous devez spécifier le nom !",
				"min_length"	=> "Le nom doit contenir au minimum 2 caractères !",
				"max_length"	=> "Le nom doit contenir au maximum 200 caractères !"
			));
		
			if($this->form_validation->run() == FALSE){
				$data['id'] = $id;
				$this->load->template("profile/edit_form", $data);
			}
			else{
				$firstname = $this->input->post("firstname");
				$lastname = $this->input->post("lastname");
				$mail = $this->session->userdata("mail");
				
				$this->user_model->edit($id == null ? $mail : $target['mail'], $firstname, $lastname);
				
				if($id == null){
					redirect("/profile");
				}
				else{
					redirect("/profile/view/". $id);
				}
			}
		}
		
		public function editPassword($id = null){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->library("form_validation");
			
			if($id != null)
			{
				needPrivileges(array("admin"));
			}
		
			$this->form_validation->set_rules('password', 'nouveau mot de passe', 'required', array(
				"required"		=> "Vous devez spécifier un mot de passe !"
			));
			
			$this->form_validation->set_rules('confirmation', 'Confirmation', 'required|matches[password]', array(
				"required"		=> "Vous devez spécifier la confirmation !",
				"matches"		=> "La confirmation et le mot de passe de correspondent pas !"
			));
			
			if($this->form_validation->run() == FALSE){
				$data['id'] = $id;
				$this->load->template("profile/password_form", $data);
			}
			else{
				$newPassword = $this->input->post("password");
				$oldPassword = $this->input->post("old");
				
				$this->load->model("user_model");
				
				if($id != null){
					$target = $this->user_model->getUserById($id);
					$target = $target;
				}
				
				if($this->user_model->login($this->session->userdata("mail"), $oldPassword) || $id != null){
					$this->load->model("category_model");
					$this->load->model("shopping_model");
					$backId = $id;
					if($id == null){
						$users = $this->user_model->getUserByMail($this->session->userdata("mail"));
						$user = $users[0];
						$id = $user['id'];
					}
					$this->category_model->changeKey($id, $this->user_model->hashPassword($newPassword));
					foreach($user->ownShoppingList as $shopping){
						$this->shopping_model->changeItemsKey($this->session->userdata("id"), $shopping->id, $this->user_model->hashPassword($newPassword));
					}
					$this->user_model->changePass($backId == null ? $this->session->userdata("mail") : $target['mail'], $newPassword);
					if($backId == null){
						redirect("/profile");
					}
					else{
						redirect("/profile/view/". $id);
					}
				}
				else{
					$data['error'] = "Mauvais passwd";
					$this->load->template("profile/password_form", $data);
				}
			}
		}
		
		public function remove($id = null){
			$this->load->helper("user");
			
			needPrivileges(array("logged_in"));
			
			if($id != null){
				needPrivileges(array("admin"));
			}
			
			$this->load->model("user_model");
			
			if($id != null){
				$targets = $this->user_model->getUserById($id);
				$target = $targets;
			}
			
			$mail = $id == null ? $this->session->userdata("mail") : $target['mail'];
			
			$this->user_model->removeByMail($mail);
			
			if($id != null){
				redirect("/administration/users");
			}
			else{
				redirect("/profile/logout");
			}
		}
		
		public function login(){
			$this->load->helper("user");
			needPrivileges(array("logged_out"));
			$this->load->library("form_validation");
			$this->load->model("user_model");
			$this->load->helper("url");
			
			$this->form_validation->set_rules('mail', 'Mail', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() == FALSE){
				$data['error'] = $this->form_validation->error_array();
				$this->load->template("profile/login_form", $data);
			}
			else{
				$mail = $this->input->post("mail");
				$password = $this->input->post("password");
				if($users = $this->user_model->login($mail, $password)){
					if($users[0]['confirmation'] == 1){
						$this->load->helper("cookie");
						if($this->input->post("stay") == 'true'){
							setcookie("userId", $users[0]['id'], time()+365*24*60*60, "/");
							setcookie("userHash", $users[0]['hash'], time()+365*24*60*60, "/");
						}
						$this->session->set_userdata(array(
							"logged_in"	=> true,
							"mail"		=> $mail,
							"id"		=> $users[0]['id'],
							"admin"		=> $users[0]['admin']
						));
						redirect("/");
					}else{
						$data['error'] = "fail";
						$this->load->template("profile/login_form", $data);
					}
				}
				else{
					$data['error'] = "fail";
					$this->load->template("profile/login_form", $data);
				}
			}
		}
		
		public function addAdmin($id){
			$this->load->helper("user");
			
			needPrivileges(array("admin"));
			
			$this->load->model("user_model");
			
			$this->user_model->editAdmin($id, true);
			
			redirect("/administration/users");
		}
		
		public function removeAdmin($id){
			$this->load->helper("user");
			
			needPrivileges(array("admin"));
			
			$this->load->model("user_model");
			
			$this->user_model->editAdmin($id, false);
			
			redirect("/administration/users");
		}
		
		public function logout(){
			$this->load->helper("url");
			$this->load->helper("user");
			
			setcookie("userId", $users[0]['id'], -1, "/");
			setcookie("userHash", $users[0]['hash'], -1, "/");
			
			needPrivileges(array("logged_in"));
			$this->session->sess_destroy();
			redirect("/");
		}
	}
?>