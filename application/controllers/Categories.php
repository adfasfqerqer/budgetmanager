﻿<?php
	class Categories extends CI_Controller{
		
		public function index(){
			$this->load->helper("url");
			redirect("reports");
		}
		
		public function add(){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->library("form_validation");
			
			$this->form_validation->set_rules("name", 'Nom', 'required|min_length[3]|max_length[30]', array(
				"required"			=> "La catégorie doit avoir un nom !",
				"min_length"		=> "Le nom de la catégorie doit compter au minimum 3 caractères !",
				"max_length"		=> "Le nom de la catégorie ne peut pas dépasser les 30 caractères !"
			));
			
			$this->form_validation->set_rules("color", 'Couleur', 'required|callback__isHexa', array(
				"required"			=> "Vous devez spécifier une couleur !",
				"callback__isHexa"	=> "Vous devez rentrer un code couleur en hexadécimal exemple: #000000"
			));
			
			$this->form_validation->set_rules("limit", 'Limite', 'integer', array(
				"integer"			=> "Vous devez spécifier un nombre sans virgule !"
			));
			
			if($this->form_validation->run() == FALSE){
				$this->load->template("categories/add_form");
			}
			else{
				$this->load->model("category_model");
				$this->load->helper("url");
				
				$name = $this->input->post("name");
				$limit = empty($this->input->post("limit")) ? 0 : $this->input->post("limit");
				$color = $this->input->post("color");
				$this->category_model->add($name, $color, $limit, $this->session->userdata("id"));
				
				redirect("categories");
			}
		}
		
		public function remove($id){
			$this->load->helper("user");
			needPrivileges(array("logged_in"));
			
			$this->load->helper("url");
			
			$this->load->model("category_model");
			
			$this->category_model->remove($id, $this->session->userdata("id"));
			
			redirect("categories");
		}
		
		public function _isHexa($str){
			if(preg_match("#\#[0-9a-f]{6}#", $str)){
				return true;
			}
			else{
				return false;
			}
		}
	}
?>