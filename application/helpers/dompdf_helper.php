<?php
	function pdf_create($html, $filename="", $stream="true"){
		$CI =& get_instance();
		$CI->load->helper("url");
		
		require_once(FCPATH."vendor/dompdf/dompdf/dompdf_config.inc.php");
		
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->render();
		if($stream){
			$dompdf->stream($filename. ".pdf");
		}
		else{
			return $dompdf->output();
		}
	}
?>