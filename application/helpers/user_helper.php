<?php
	
	function needPrivileges($privileges){
		$CI =& get_instance();
		$CI->load->library("session");
		$CI->load->helper("url");
		
		if(!is_array($privileges)){
			throw new InvalidArgumentException("The privileges attributes must be an array of privileges");
		}
		foreach($privileges as $privilege){
			switch($privilege){
				case "logged_out":
				if(empty($CI->session->userdata("logged_in")) || $CI->session->userdata("logged_in") == false){
					break;
				}
				else{
					redirect("/");
				}
				break;
				
				case "logged_in":
					if(!empty($CI->session->userdata("logged_in")) && $CI->session->userdata("logged_in") == true){
						break;
					}
					else{
						redirect("/profile/login");
					}
				break;
				
				case "admin":
					if(!empty($CI->session->userdata("logged_in")) && $CI->session->userdata("logged_in") == true){
						if($CI->session->userdata("admin")){
							break;
						}
						else{
							redirect("/errors/admin");
						}
					}
					else{
						redirect("/profile/login");
					}
				break;
			}
		}
	}
?>