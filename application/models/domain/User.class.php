﻿<?php
	class User{
		public $mail;
		public $firstname;
		public $lastname;
		public $hash;
		public $confirmation;
		
		public function User($mail, $firstname, $lastname, $hash, $confirmation){
			$this->mail = $mail;
			$this->firstname = $firstname;
			$this->lastname = $lastname;
			$this->hash = $hash;
			$this->confirmation = $confirmation;
		}
	}
?>