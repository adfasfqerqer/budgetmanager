<?php
	class Shopping_model extends CI_Model{
		
		public function __construct(){
			$this->load->library("encryption");
		}
		
		public function getItem($id){
			$item = R::findOne("items", "id = ?", array($id));
			return $item;
		}
		
		public function getShoppingById($id){
			$shopping = R::findOne("shopping", "id = ?", array($id));
			return $shopping;
		}
		
		public function getUserShoppings($userId){
			$user = R::findOne("users", "id = ?", array($userId));
			
			$shoppings = $user->with("ORDER BY date DESC")->ownShoppingList;
			
			return $shoppings;
		}
		
		public function getItemsFromCategory($catId, $userId){
			$category = R::findOne("categories", "id = ?", array($catId));
			$items = $category->ownItemsList;
			foreach($items as $key1 => $item){
				foreach($items[$key1] as $key2 => $value){
					$items[$key1][$key2] = !empty($this->encryption->my_decrypt($userId, $value)) ? $this->encryption->my_decrypt($userId, $value) : $items[$key1][$key2];
				}
			}
			return $items;
		}
		
		public function getTotalPriceOfACategory($categoryId, $userId){
			$category = R::findOne("categories", "id = ?", array($categoryId));
			
			$items = $category->ownItemsList;
			$total = 0;
			foreach($items as $item){
				$total += $this->encryption->my_decrypt($userId, $item->price) * $this->encryption->my_decrypt($userId, $item->quantity);
			}
			
			return $total;
		}
		
		public function addItems($items, $userId){
			$date = time();
			$batch = array();
			
			$user = R::findOne("users", "id = ?", array($userId));
			
			$shopping = R::dispense("shopping");
			$shopping->date = $date;
			$shopping->users = $user;
			
			R::store($shopping);
			
			foreach($items as $key => $item){
				$batch[$key]['name'] = $this->encryption->my_encrypt($userId, $item['name']);
				$batch[$key]['price'] = $this->encryption->my_encrypt($userId, $item['price']);
				$batch[$key]['quantity'] = $this->encryption->my_encrypt($userId, $item['quantity']);
				$batch[$key]['categories_id'] = $item['category'];
				$batch[$key]['shopping_id'] = $shopping->id;
			}
			
			$this->db->insert_batch("items", $batch);
		}
		
		public function changeItemsKey($userId, $shoppingId, $newHash){
			$shopping = $this->getShoppingById($shoppingId);
			
			foreach($shopping->ownItemsList as $item){
				$newData = $this->encryption->changeKey($userId, array("name" => $item->name, "price" => $item->price, "quantity" => $item->quantity), $newHash);
				$item->name = $newData['name'];
				$item->price = $newData['price'];
				$item->quantity = $newData['quantity'];
				
				R::store($item);
			}
		}
	}
?>