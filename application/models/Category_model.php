<?php
	class Category_model extends CI_Model{
		
		public function __construct(){
			parent::__construct();
			$this->load->library("encryption");
		}
		
		public function getAllOfUser($userId){
			$this->load->model("shopping_model");
			
			$user = R::findOne("users", "id = ?", array($userId));
			$categories = $user->ownCategoriesList;
			
			foreach($categories as $key => $category){
				$categories[$key]["name"] = $this->encryption->my_decrypt($userId, $category['name']);
				$categories[$key]["price_limit"] = $this->encryption->my_decrypt($userId, $category['price_limit']);
				$categories[$key]["items"] = $this->shopping_model->getItemsFromCategory($category['id'], $userId);
			}
			return $categories;
		}
		
		public function getCategory($categoryId, $userId){
			$category = R::findOne("categories", "id=?", array($categoryId));
			
			$category->price_limit = $this->encryption->my_decrypt($userId, $category->price_limit);
			$category->name = $this->encryption->my_decrypt($userId, $category->name);
			
			return $category;
		}
		
		public function add($name, $color, $limit, $userId){
			//$this->db->set("name", $this->encryption->my_encrypt($userId, $name))->set("color", $color)->set("price_limit", $this->encryption->my_encrypt($userId, $limit))->set("fk_users", $userId)->insert("categories");
			
			$user = R::findOne("users", "id = ?", array($userId));
			
			$category = R::dispense("categories");
			$category->name = $this->encryption->my_encrypt($userId, $name);
			$category->color = $color;
			$category->price_limit = $this->encryption->my_encrypt($userId, $limit);
			$category->users = $user;
			
			R::store($category);
		}
		
		public function remove($id, $userId){
			//$this->db->where('id', $id)->where("fk_users", $userId)->delete("categories");
			$category = R::findOne("categories", "id = ? AND users_id = ?", array($id, $userId));
			$category->xownItemsList = array();
			R::store($category);
			R::trash($category);
		}
		
		public function changeKey($userId, $newHash){
			$user = R::findOne("users", "id = ?", array($userId));
			$categories = $user->ownCategoriesList;
			foreach($categories as $key => $category){
				$newData = $this->encryption->changeKey($userId, array("name" => $category['name'], "price_limit" => $category['price_limit']), $newHash);
				$category->name = $newData['name'];
				$category->price_limit = $newData['price_limit'];
				R::store($category);
			}
		}
		
		public function userOwnCategory($userId, $categoryId){
			$category = $this->db->select("*")->from("categories")->where("id", $categoryId)->get()->result_array();
			return !empty($category) && ($category[0]['users_id'] == $userId);
		}
	}
?>