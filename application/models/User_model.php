﻿<?php
	require_once("domain/User.class.php");

	class User_model extends CI_Model{
		
		public function __construct(){
			parent::__construct();
		}
		
		public function removeByMail($mail){
			$user = R::findOne("users", "mail = ?", array($mail));
			R::trash($user);
			//$this->db->delete("users", array("mail" => $mail));
		}
		
		public function getAll(){
			return R::findAll("users");
			//return $this->db->select("*")->from("users")->get()->result_array();
		}
		
		public function editAdmin($id, $admin){
			$user = R::findOne("users", "id = ?", array($id));
			$user->admin = $admin;
			R::store($user);
		}
		
		public function login($mail, $password){
			//$match = $this->db->select("*")->from("users")->where("mail", $mail)->where("hash", $this->hashPassword($password))->get()->result_array();
			$match = R::findOne("users", "mail = ? AND hash = ?", array($mail, $this->hashPassword($password)));
			$matches[0] = $match;
			return !empty($matches) ? $matches : false;
		}
		
		public function loginHash($id, $hash){
			//$match = $this->db->select("*")->from("users")->where("id", $id)->where("hash", $hash)->get()->result_array();
			$match = R::findOne("users", "id = ? AND hash = ?", array($id, $hash));
			$matches[0] = $match;
			return !empty($matches) ? $matches : false;
		}
		
		public function edit($mail, $firstname, $lastname){
			// $this->db->set('firstname', $firstname)
					// ->set('lastname', $lastname)
					// ->where('mail', $mail)
					// ->update('users');
			$user = R::findOne("users", "mail = ?", array($mail));
			$user->lastname = $lastname;
			$user->firstname = $firstname;
			R::store($user);
		}
		
		public function changePass($mail, $newPass){
			//$this->db->set('hash', $this->hashPassword($newPass))->where("mail", $mail)->update("users");
			$user = R::findOne("users", "mail = ?", array($mail));
			$user->hash = $this->hashPassword($newPass);
			
			R::store($user);
		}
			
		public function userIsUnique($mail){
			$user = R::find("users", "mail = ?", array($mail));
			if(empty($user)){
				return true;
			}
			else{
				return false;
			}
		}
		
		public function getUserByMail($mail){
			$user = R::findOne("users", "mail = ?", array($mail));
			$users[0] = $user;
			return $users;
		}
		
		public function getUserById($id){
			$users = R::findOne("users", "id = ?", array($id));
			return $users;
		}
		
		public function register($mail, $firstname, $lastname, $password){
			$confirmToken = sha1(uniqid().$mail.$firstname);
			
			$user = R::dispense("users");
			$user->mail = $mail;
			$user->firstname = $firstname;
			$user->lastname = $lastname;
			$user->hash = $this->hashPassword($password);
			$user->confirmation = $confirmToken;
			
			R::store($user);
			
			return $confirmToken;
		}
		
		public function hashPassword($password){
			$hash = sha1($this->config->item("encryption_key").$password);
			return sha1($hash);
		}
		
		public function verify($token){
			//$this->db->set("confirmation", true)->where("confirmation", $token)->update("users");
			$user = R::findOne("users", "confirmation = ?", array($token));
			$user->confirmation = true;
			R::store($user);
		}
	}
?>