﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<?php echo $captcha_linker ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Inscription</h4>
			</div>
			<div class="panel-body">
				<?php echo form_open("register/submit") ?>
					<div class="form-group <?php echo !empty(form_error("mail")) ? "has-error" : "" ?>">
						<input name="mail" type="text" class="form-control" placeholder="Adresse e-mail" />
						<small style="color:red"><?php echo form_error("mail") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("firstname")) ? "has-error" : "" ?>">
						<input name="firstname" type="text" class="form-control" placeholder="Prénom" />
						<small style="color:red"><?php echo form_error("firstname") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("lastname")) ? "has-error" : "" ?>">
						<input name="lastname" type="text" class="form-control" placeholder="Nom" />
						<small style="color:red"><?php echo form_error("lastname") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("password")) ? "has-error" : "" ?>">
						<input name="password" type="password" class="form-control" placeholder="Mot de passe" />
						<small style="color:red"><?php echo form_error("password") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("confirmation")) ? "has-error" : "" ?>">
						<input name="confirmation" type="password" class="form-control" placeholder="Confirmez le mot de passe" />
						<small style="color:red"><?php echo form_error("confirmation") ?></small>
					</div>
					<div class="form-group">
						<?php echo $captcha ?>
					</div>
					<div class="form-group <?php echo !empty(form_error("cgu")) ? "has-error" : "" ?>">
						<input name="cgu" type="checkbox" /> <label>J'ai lu et accepte les <a href="#">CGU</a></label>
						<small style="color:red"><?php echo form_error("cgu") ?></small>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-default" value="S'enregistrer" />
					</div>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>