﻿<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h4>Inscription réussie !</h4>
			</div>
			<div class="panel-body">
				<p>Votre inscription a été confirmée, merci d'utiliser ce service !</p>
			</div>
		</div>
	</div>
</div>