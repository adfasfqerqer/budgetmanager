<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h4>Validez votre inscription</h4>
			</div>
			<div class="panel-body">
				<p>Votre inscription a été enregistrée, un mail contenant un lien de vérification vous a été envoyé, merci de cliquer sur ce lien.</p>
			</div>
		</div>
	</div>
</div>