﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper("url");
?>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Liste des utilisateurs</h4>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<thead>
						<tr><th>Adresse e-mail</th><th>Prénom</th><th>Nom</th><th>Confirmé</th><th></th><th></th><th></th></tr>
					</thead>
					<tbody>
						<?php foreach($users as $user): ?>
							<tr>
								<td><?php echo $user['mail'] ?></td>
								<td><?php echo $user['firstname'] ?></td>
								<td><?php echo $user['lastname'] ?></td>
								<td><?php echo $user['confirmation'] == 1 ? "Confirmé" : "En attente" ?></td>
								<td><?php echo $user['admin'] ? '<a href="' .site_url("profile/removeAdmin/". $user['id']). '"><i class="fa fa-user-secret"></i></a>' : '<a href="' .site_url("profile/addAdmin/". $user['id']). '"><i class="fa fa-user"></i></a>'?></td>
								<td><a href="<?php echo site_url("profile/view/". $user['id']) ?>"><i class="fa fa-eye"></i></a></td>
								<td><a href="<?php echo site_url("profile/remove/". $user['id']) ?>" data-confirm="Êtes-vous certain de vouloir supprimer cet utilisateur ainsi que toutes ses données ?"><i style="color:#D9534F;" class="fa fa-trash"></i></a></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>