﻿<?php
	$this->load->helper("url");
	$this->load->library("encryption");
?>
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
			 <h4>Derniers achats - <?php echo $category->name ?></h4>
			</div>
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<th>
								Date de l'achat
							</th>
							<th>
								Nom de l'objet
							</th>
							<th>
								Prix
							</th>
							<th>
								Quantité
							</th>
							<th>
								Sous-total
							</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($category->ownItemsList as $item): ?>
						<tr>
							<td><?php echo date("d-m-Y H:i", $item->shopping->date) ?></td>
							<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->name) ?></td>
							<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->price) ?></td>
							<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) ?></td>
							<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) *  $this->encryption->my_decrypt($this->session->userdata("id"), $item->price) ?></td>
						</tr>
					<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Evolution</h4>
					</div>
					<div class="panel-body">
						<canvas id="evolution_chart"></canvas>
					</div>
				</div>
			</div>
		</div>
		<!--<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Situation globale</h4>
					</div>
					<div class="panel-body">
					
					</div>
				</div>
			</div>
		</div>-->
	</div>
</div>
<script src="<?php echo site_url("/js/chart/chart.min.js") ?>"></script>
<script>
	var ctx = $("#evolution_chart").get(0).getContext("2d");
	var evolution_chart = new Chart(ctx).Line({
		labels: [<?php echo "'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'";?>],
		datasets:[
		{
			label: "Evolution",
			fillColor: "RGBA(156, 169, 94, 0.5)",
			strokeColor: "RGBA(106, 183, 182, 1)",
			pointColor: "RGBA(106, 183, 182, 1)",
			data: [<?php foreach($totalPerMonth as $total):?> <?php echo $total ?>, <?php endforeach ?>
				
			]
			
		}
		]
	});
</script>