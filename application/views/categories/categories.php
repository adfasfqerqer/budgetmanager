﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<h3>Résumé</h3>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Evolution</h4>
			</div>
			<div class="panel-body">
				<canvas id="evolution_chart"></canvas>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default" style="max-height:400px;overflow:auto;">
			<div class="panel-heading">
				<h4>Derniers achats</h4>
			</div>
			<div class="panel-body">
				<?php foreach($shoppings as $shopping): ?>
					<h4><?php echo date("d.m.Y - H:i", $shopping->date) ?></h4>
					<table class="table">
						<thead>
							<tr>
								<th>Objet</th><th>Prix</th><th>Quantité</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($shopping->ownItemsList as $item): ?>
								<tr>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->name) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->price) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
					<hr />
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Situation globale</h4>
			</div>
			<div class="panel-body">
				<canvas id="situation_chart"></canvas>
			</div>
		</div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<h3>Catégories</h3>
	<?php $i = 0; ?>
	<?php foreach($categories as $key => $category): ?>
	<?php $i++ ?>
	<div class="col-md-4">
		<div class="panel panel-default" style="border-color: <?php echo $category['color'] ?>">
			<div style="background-color:<?php echo $category['color'] ?>" class="panel-heading">
				<h4><?php echo $category['name'] ?></h4>
			</div>
			<div class="panel-body">
				<table class="table">
					<tbody>
						<tr><td>Coûts</td><td><?php $sum = 0;foreach($category['items'] as $item): $sum += $item['price'] * $item['quantity']; endforeach;?><span style="color: <?php echo $sum >= $category['price_limit'] && !empty($category['price_limit']) ? "red" : "auto" ?>">CHF <?php echo $sum ?></span></td></tr>
						<?php if(!empty($category['price_limit'])): ?><tr><td>Limite</td><td>CHF <?php echo $category['price_limit'] ?></td></tr><?php endif ?>
					</tbody>
				</table>
				<a href="<?php echo site_url("reports/category/". $category['id']) ?>">Détails</a>
				<span style="float:right">
					<a style="color:red;" href="<?php echo site_url("categories/remove/". $category['id']) ?>" data-confirm="Êtes-vous certain de vouloir supprimer cette catégorie et tous les achats qui y sont attachés ?"><i class="fa fa-trash"></i></a>
				</span>
			</div>
		</div>
	</div>
	<?php if((($i) % 3) == 0): ?>
		</div>
		<div class="row">
	<?php endif ?>
	<?php endforeach ?>
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Nouvelle catégorie</h4>
			</div>
			<div class="panel-body">
				<a href="<?php echo site_url("categories/add") ?>">Ajouter une catégorie</a>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo site_url("/js/chart/chart.min.js") ?>"></script>
<script>
	var ctx = $("#evolution_chart").get(0).getContext("2d");
	var evolution_chart = new Chart(ctx).Line({
		labels: [<?php if($sampling == "year") echo "'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'"; else echo "'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'"; ?>],
		datasets:[
		{
			label: "Evolution",
			fillColor: "RGBA(156, 169, 94, 0.5)",
			strokeColor: "RGBA(106, 183, 182, 1)",
			pointColor: "RGBA(106, 183, 182, 1)",
			data: [<?php if($sampling == "year"): foreach($totalPerMonth as $total):?> <?php echo $total ?>, <?php endforeach ?><?php endif ?>
				
			]
			
		}
		]
	});
	var ctx_situation = $("#situation_chart").get(0).getContext("2d");
	var situation_chart = new Chart(ctx_situation).Pie([
	<?php foreach($categories as $category): ?>
	<?php $sum = 0; ?>
	<?php foreach($category->ownItemsList as $item): ?>
	<?php $sum += $this->encryption->my_decrypt($this->session->userdata("id"), $item->price) * $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) ?>
	<?php endforeach ?>
	{
		label: "<?php echo $category->name ?> (CHF)",
		value: <?php echo $sum ?>,
		color: "<?php echo $category->color ?>"
	},
	<?php endforeach ?>
	])
</script>