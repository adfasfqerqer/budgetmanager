﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Ajout d'une catégorie</h4>
			</div>
			<div class="panel-body">
				<?php echo form_open("categories/add") ?>
					<div class="form-group <?php echo !empty(form_error("name")) ? "has-error" : "" ?>">
						<input name="name" class="form-control" placeholder="Nom de la catégorie" />
						<small style="color:red"><?php echo form_error("name") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("color")) ? "has-error" : "" ?>">
						<label>Apparence</label>
						<div class="input-group" id="colorpicker">
							<input name="color" class="form-control" placeholder="couleur" />
							<span class="input-group-addon"><i></i></span>
						</div>
						<small style="color:red"><?php echo form_error("color") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("limit")) ? "has-error" : "" ?>">
						<label>Limite de dépense</label>
						<div class="input-group">
							<span class="input-group-addon">CHF</span>
							<input name="limit" type="number" class="form-control" placeholder="Limite à ne pas dépasser" />
							<span class="input-group-addon">.00</span>
						</div>
						<small>Laisser vide ou à 0 pour une catégorie sans limite</small>
						<small style="color:red"><?php echo form_error("limit") ?></small>
					</div>
					<input type="submit" class="btn btn-default" value="Créer" />
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>

<script>
    $(function(){
        $('#colorpicker').colorpicker();
    });
</script>