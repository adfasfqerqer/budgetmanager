<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->library("encryption") ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Résumé des achats</h4>
			</div>
			<div class="panel-body">
				<?php foreach($shoppings as $shopping): ?>
					<h4><?php echo date("d m Y - H:i", $shopping['date']) ?></h4><hr />
					<table class="table">
						<thead>
							<tr>
								<th>Objet</th><th>Catégorie</th><th>Coût</th><th>Quantité</th><th>Sous-total</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($shopping->ownItemsList as $item): ?>
								<tr>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->name) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->categories->name) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->price) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) ?></td>
									<td><?php echo $this->encryption->my_decrypt($this->session->userdata("id"), $item->quantity) * $this->encryption->my_decrypt($this->session->userdata("id"),$item->price) ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>