﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Ajout d'un achat</h4>
			</div>
			<div class="panel-body">
				<small style="color:red"><?php echo validation_errors() ?></small>
					<?php echo form_open("shopping/add") ?>
							<div class="row">
								<div class="col-xs-3">
									<label>Nom</label>
								</div>
								<div class="col-xs-3">
									<label>Catégorie</label>
								</div>
								<div class="col-xs-2">
									<label>Prix</label>
								</div>
								<div class="col-xs-2">
									<label>Quantité</label>
								</div>
							</div>
							<div id="form_container">
								<div class="form-group" id="firstLine">
									<div style="margin-top: 10px" class="row">
										<div class="col-xs-3">
											<input class="form-control" placeholder="Objet" name="name[]" />
										</div>
										<div class="col-xs-3">
											<select onchange="needRedirect(value)" name="category[]" class="form-control">
												<?php foreach($categories as $category): ?>
													<option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
												<?php endforeach ?>
												<option value="new_category_redirect">Nouvelle catégorie</option>
											</select>
										</div>
										<div class="col-xs-2">
												<input name="price[]" type="number" value="1" min="0" step="0.05" data-number-to-fixed="2"  class="form-control currency" placeholder="Prix" />
										</div>
										<div class="col-xs-2">
											<input name="quantity[]" type="number" min="0" value="1" class="form-control" placeholder="Qté" />
										</div>
										<div class="col-xs-2">
											<a id="add_button" href="#">add</a>
										</div>
									</div>
								</div>
							</div>
							<div class="row" style="margin-top:10px">
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" class="btn btn-default" value="Valider" />
									</div>
								</div>
							</div>
					<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>

<script>
	var template = $("#firstLine").clone();
	var formContainer = $("#form_container");
	
	$("#add_button").click(function(){
		copyLine();
		refreshForm();
	});
	
	function copyLine(){
		$("#add_button").remove();
		formContainer.append(template.html());
		
		$("#add_button").click(function(){
			copyLine();
		});
	}
	
	function needRedirect(str){
		if(str == "new_category_redirect"){
			document.location.href="<?php echo site_url("categories/add") ?>"
		}
	}
</script>

<script>
	webshims.setOptions('forms-ext', {
		replaceUI: 'auto',
		types: 'number'
	});
	function refreshForm(){
		webshims.polyfill('forms forms-ext');
	}
	refreshForm();
</script>