﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Modifier les informations</h4>
			</div>
			<div class="panel-body">
				<?php echo form_open("profile/edit/". @$id) ?>
					<div class="form-group <?php echo !empty(form_error("firstname")) ? "has-error" : "" ?>">
						<input name="firstname" class="form-control" placeholder="Prénom" />
						<small style="color:red"><?php echo form_error("firstname") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("lastname")) ? "has-error" : "" ?>">
						<input name="lastname" class="form-control" placeholder="Nom" />
						<small style="color:red"><?php echo form_error("lastname") ?></small>
					</div>
					<input type="submit" class="btn btn-default" value="Valider" />
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>