﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Connexion</h4>
			</div>
			<div class="panel-body">
				<?php echo form_open("profile/login") ?>
					<div class="form-group">
						<input name="mail" class="form-control" placeholder="Adresse e-mail" />
					</div>
					<div class="form-group">
						<input name="password" type="password" class="form-control" placeholder="Password" />
					</div>
					<input type="submit" class="btn btn-default" value="Connexion" />
					<input name="stay" id="stayLogged" type="checkbox" value="true" />
					<label style="font-weight: normal;" for="stayLogged">Rester connecté</label>
					<p>Vous n'avez pas de compte ? <br /><a href="<?php echo site_url("register") ?>">S'enregistrer</a></p>
					<div>
						<p><small style="color:red;"><?php echo !empty($error) ? "La connexion a échouée" : "" ?></small></p>
					</div>
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>