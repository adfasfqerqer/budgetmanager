﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Changement de mot de passe</h4>
			</div>
			<div class="panel-body">
				<?php echo form_open("profile/editPassword/". @$id) ?>
					<div class="form-group <?php echo !empty($error) ? "has-error" : "" ?>">
						<input name="old" type="password" class="form-control" placeholder="Ancien mot de passe" />
						<small style="color:red"><?php echo form_error("old") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("password")) ? "has-error" : "" ?>">
						<input name="password" type="password" class="form-control" placeholder="Nouveau mot de passe" />
						<small style="color:red"><?php echo form_error("password") ?></small>
					</div>
					<div class="form-group <?php echo !empty(form_error("confirmation")) ? "has-error" : "" ?>">
						<input name="confirmation" type="password" class="form-control" placeholder="Confirmez le mot de passe" />
						<small style="color:red"><?php echo form_error("confirmation") ?></small>
					</div>
					<input type="submit" class="btn btn-default" value="Valider" />
				<?php echo form_close() ?>
			</div>
		</div>
	</div>
</div>