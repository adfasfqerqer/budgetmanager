﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php if(empty($user)) redirect("/profile") ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4><?php echo $admin ? "Profil de ". $user['firstname']. " ". $user['lastname'] : "Mon profil" ?></h4>
			</div>
			<div class="panel-body">
				<p>Voici les informations relatives à votre profil :</p>
				<table class="table">
					<tr><td>Adresse e-mail</td><td><b><?php echo $user['mail'] ?></b></td></tr>
					<tr><td>Prénom</td><td><b><?php echo $user['firstname'] ?></b></td></tr>
					<tr><td>Nom</td><td><b><?php echo $user['lastname'] ?></b></td></tr>
					<tr><td>Password</td><td><a href="<?php echo $admin ? site_url("profile/editPassword/". $id) : site_url("profile/editPassword") ?>"><button class="btn btn-default">Changer le mot de passe</button></a></td><tr>
				</table>
				<p>Pour les modifier, veuillez <a href="<?php echo $admin ? site_url("profile/edit/". $id) : site_url("/profile/edit") ?>">cliquer ici</a></p>
			</div>
		</div>
	</div>	
</div>