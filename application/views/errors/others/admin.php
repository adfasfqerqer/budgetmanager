﻿<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->helper('url'); ?>
<?php $this->load->helper('form'); ?>
<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h4>Vous n'avez pas accès à cette ressource !</h4>
			</div>
			<div class="panel-body">
				<p>L'accès à cette ressource nécessite des droits d'administrateurs !</p>
			</div>
		</div>
	</div>
</div>