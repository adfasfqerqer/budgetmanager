﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper("url");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url("/icon") ?>/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo site_url("/icon") ?>/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo site_url("/icon") ?>/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url("/icon") ?>/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url("/icon") ?>/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo site_url("/icon") ?>/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo site_url("/icon") ?>/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo site_url("/icon") ?>/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo site_url("/icon") ?>/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo site_url("/icon") ?>/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url("/icon") ?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url("/icon") ?>/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url("/icon") ?>/favicon-16x16.png">
		<link rel="manifest" href="<?php echo site_url("/icon") ?>/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo site_url("/icon") ?>/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap.min.css") ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/main.css") ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap-colorpicker.min.css") ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url("vendor/fortawesome/font-awesome/css/font-awesome.min.css") ?>" />
		<script type="text/javascript" src="<?php echo base_url("js/jquery-2.2.0.min.js") ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/bootstrap.min.js") ?>"></script>
		<script type="text/javascript" src="<?php echo base_url("js/bootstrap-colorpicker.min.js") ?>"></script>
		<script src="<?php echo base_url("js/js-webshim/minified/polyfiller.js") ?>"></script> 

		<title>Budget Manager</title>
	</head>
	<body style="padding-top:70px;">
		<div class="container-fluid">
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="row">
						<a href="<?php echo site_url("/") ?>"><img style="padding-top:10px;" height="70px;" src="<?php echo base_url("img/logo_transparent.png") ?>" id="logo" /></a>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="<?php echo site_url("categories") ?>">Home</a></li>
							<li><a href="<?php echo site_url("shopping/add") ?>">Nouvel achat</a></li>
							<?php if(!$this->session->userdata('logged_in')): ?><li><a href="<?php echo site_url("profile/login") ?>">Connexion / Inscription</a></li>
							<?php else: ?>
							<li class="dropdown">
							  <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<?php echo $this->session->userdata("mail") ?>
								<span class="caret"></span>
								<br /><small><?php echo $this->session->userdata("admin") ? "(admin)" : "" ?></small>
							  </a>
							  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li><a href="<?php echo site_url("profile") ?>">Mon profil</a></li>
									<?php if($this->session->userdata("admin")): ?><li><a href="<?php echo site_url("administration") ?>">Administration</a></li><?php endif ?>
									<li><a href="<?php echo site_url("profile/logout") ?>">Déconnexion</a></li>
							  </ul>
							</li>
							<?php endif ?>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="container">