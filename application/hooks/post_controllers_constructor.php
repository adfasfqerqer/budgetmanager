<?php
	function getPrivileges(){
		$CI =& get_instance();
		if($CI->session->userdata("logged_in")){
			$CI->load->model("user_model");
			
			$users = $CI->user_model->getUserByMail($CI->session->userdata("mail"));
			$user = $users[0];
			$CI->session->set_userdata(array(
				"admin"	=> $user['admin'],
				"id"=> $user['id']
			));
		}
	}
	
	function loginCookies(){
		if(!empty($_COOKIE['userHash']) && !empty($_COOKIE['userId'])){
			$CI =& get_instance();
			if(!$CI->session->userdata("logged_in")){
				$CI->load->model("user_model");
				
				if($users = $CI->user_model->loginHash($_COOKIE['userId'], $_COOKIE['userHash'])){
					if($users[0]['confirmation']){
						$CI->session->set_userdata(array(
							"logged_in"	=> true,
							"mail"		=> $users[0]['mail'],
							"id"		=> $users[0]['id'],
							"admin"		=> $users[0]['admin']
						));
					}
				}
			}
		}
	}
?>