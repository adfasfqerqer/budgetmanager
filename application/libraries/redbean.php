<?php
	require_once("rb.php");
	
	class redbean{
		public function __construct(){
			$ci =& get_instance();
			
			$ci->load->database();
			
			R::setup("mysql:host=".$ci->db->hostname. ";dbname=". $ci->db->database, $ci->db->username, $ci->db->password);
		}
	}
?>