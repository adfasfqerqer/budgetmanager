<?php
	class MY_encryption extends CI_encryption{

		public function my_encrypt($userId, $data, $newHash = null){
			$ci =& get_instance();
			
			$ci->load->model("user_model");
			
			$user = $ci->user_model->getUserById($userId);
			$key = empty($newHash) ? $user['hash'] : $newHash;
			$key .= $user['id'];
			$this->initialize(array(
					'cipher' => 'aes-256',
					'mode' => 'ctr',
					'key' => md5($ci->config->item("encryption_key").$key)
			));
			return $this->encrypt($data);
		}
		
		public function my_decrypt($userId, $data, $newHash = null){
			$ci =& get_instance();
			
			$ci->load->model("user_model");
			
			$user = $ci->user_model->getUserById($userId);
			$key = empty($newHash) ? $user['hash'] : $newHash;
			$key .= $user['id'];
			$this->initialize(array(
					'cipher' => 'aes-256',
					'mode' => 'ctr',
					'key' => md5($ci->config->item("encryption_key").$key)
			));

			return $this->decrypt($data);
		}
		
		public function changeKey($userId, $data, $newHash){
			if(!is_array($data)){
				$data[0] = $data;
			}
			$newData = [];
			foreach($data as $key => $toDecrypt){
				$decryptedData = $this->my_decrypt($userId, $toDecrypt);
				$newData[$key] = $this->my_encrypt($userId, $decryptedData, $newHash);
			}
			
			return $newData;
		}
	}
?>