# Budget Manager #

## Changelog ##
### Pre-release 0.9.1 ###
* Fixed an issue on the changing password procedure that was causing lost of items
* Added a CGU checkbox on the registration
* Little design fixes
<hr />
### Pre-release 0.9.0 ###
* Core features
* First release
<hr />